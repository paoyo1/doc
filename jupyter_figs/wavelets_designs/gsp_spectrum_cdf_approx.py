from __future__ import division

import numpy as np
#from scipy import sparse
#from scikits.sparse.cholmod import cholesky-
from scipy.sparse import csc_matrix
#from scipy.sparse import identity

#from ldl_routines import ldl_symbolic, ldl_numeric
#from adapted_amd import adapted_amd

# Compute an approximation of the cumulative density function of the graph Laplacian eigenvalues
def gsp_spectrum_cdf_approx(G):
    
    #num_pts = 25
    num_pts = 8
    
    n = G['N']
    lmax = G['lmax']
    
    counts = np.zeros((num_pts,))
    counts[-1] = n - 1
    
    interp_x = np.arange(num_pts)*lmax/(num_pts-1)
    
    #I = identity(n)
    
    A = csc_matrix(G['L'])
    #Ap = A.indptr
    #Ai = A.indices
    
    #P = adapted_amd(n, A)
    
    #Lp,Parent,Pinv = ldl_symbolic(n, Ap, Ai, P)
    
    for i in range(1,num_pts-1):
        
        #shift_matrix = csc_matrix(interp_x[i]*I)
        
        #mat = A - shift_matrix
        
        #mat_Ap = mat.indptr
        #mat_Ai = mat.indices
        #mat_Ax = mat.data
        
        #D = ldl_numeric(n, mat_Ap, mat_Ai, mat_Ax, Lp, Parent, P, Pinv)
        
        #D2 = np.linalg.eigvalsh(mat.toarray())
        
        D3 = np.linalg.eigvalsh(A.toarray())
        
        #print 'D: ', np.sum(D<0), 'D2: ', np.sum(D2<0), 'D3: ', np.sum(D3<interp_x[i])
        
        #counts[i] = np.sum(D<0)
        #counts[i] = np.sum(D2<0)
        counts[i] = np.sum(D3<interp_x[i])
    
    interp_y = counts/(n-1)
    
    approx_spectrum = dict()
    approx_spectrum['x'] = interp_x
    approx_spectrum['y'] = interp_y
    
    return(approx_spectrum)