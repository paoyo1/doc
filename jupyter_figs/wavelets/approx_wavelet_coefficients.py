from __future__ import division

import time
from math import pi
import numpy as np
from scipy.integrate import trapz
from scipy.sparse.linalg import eigsh

from wavelets.filters import filter_bank_design


def coef_c(M, m, alpha, kernel='abspline3'):
    theta = np.arange(0, pi+0.001, pi/50)
    g = filter_bank_design(2*alpha, m-1, kernel) #'meyer'
    c = np.zeros(shape=(m, M))
    a = alpha*(np.cos(theta)+1)
    for j in range(m):
        b = g[j](a)
        for k in range(M):
            d = np.cos(k * theta)
            y = d * b
            c[j][k] = (2/pi) * trapz(y, x=theta)
    return c

def pol_chebyshev(f, L, M, alpha):

    N = len(f)

    pol = np.zeros(shape=(M, N))

    pol[0, :] = f
    sm = L.dot(f) # caro
    pol[1, :] = ((1/alpha) * sm) - f

    for k in range(2, M):
        sm = L.dot(pol[k-1, :])  # caro
        pol[k, :] = (2/alpha) * sm - 2 * pol[k-1, :] - pol[k-2, :]

    return pol

def chebyshev_approximation(f, L, m=8, M=40, kernel='abspline3'):
    N = len(f)
    lambdaMax = eigsh(L, k=1, which='LA')[0]
    #print 'lambdaMax'
    #print lambdaMax
    
    alpha = lambdaMax * 0.5

    pol = pol_chebyshev(f, L, M, alpha)

    c = coef_c(M, m, alpha, kernel)
    w = np.zeros(shape=(m, N))
    for i in range(N):
        for j in range(m):
            sm = 0
            sm += 0.5 * c[j][0] * f[i]
            sm += np.dot(c[j, 1:M], pol[1:M, i]) # caro

            w[j][i] = sm

    return w

def approx_wavelets_coefficients(f, L, m=8, M=40, kernel='abspline3'):
    start = time.time()
    w = chebyshev_approximation(f, L, m, M, kernel)
    approximation_time = time.time()-start
    return w,approximation_time
    
