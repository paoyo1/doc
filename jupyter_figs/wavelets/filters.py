from __future__ import division

import numpy as np
import scipy.optimize as opt

def filter_bank_design(lmax, nScales, designtype= 'abspline3', lpfactor=20, a=2, b=2, t1 = 1, t2 = 2):
    switcher = {
        'abspline3': abspline3(lmax, nScales, lpfactor, a, b, t1, t2),
        'mexican_hat': mexican_hat(lmax, nScales, lpfactor, a, b, t1, t2),
        'meyer': meyer(lmax, nScales, lpfactor, a, b, t1, t2),
        'simple_tf': abspline3(lmax, nScales, lpfactor, a, b, t1, t2),
    }
    
    return switcher.get(designtype)

def abspline3(lmax, nScales, lpfactor, a, b, t1, t2):
    lmin = lmax / lpfactor
    t = log_scales(lmin,lmax,nScales)
    #print t
    
    gb = lambda x: kernel_abspline3(x, a, b, t1, t2)
    g = [None]*(nScales+1)
    for j in range(nScales):
        g[j+1] = lambda x, t=t[j]: gb(t * x)
        
    f = lambda x: - gb(x)
    xstar = opt.fminbound(f, 1, 2)

    gamma_l = - f(xstar)
    lminfac = 0.6 * lmin
    
    gl = lambda x: np.exp(- x ** 4)
    g[0] = lambda x: gamma_l * gl(x / lminfac)
    return g
    
def mexican_hat(lmax, nScales, lpfactor, a, b, t1, t2):
    lmin = lmax / lpfactor
    t = log_scales(lmin,lmax,nScales)
    
    gb= lambda x: x*np.exp(-x)
    
    g = [None]*(nScales+1)
    for j in range(nScales):
        g[j+1] = lambda x, t=t[j]: gb(t * x)

    lminfac=.4*lmin;
    gl = lambda x:  np.exp(-x ** 4)
    g[0] = lambda x: 1.2*np.exp(-1) * gl(x / lminfac);    
    
    return g
    
def meyer(lmax, nScales, lpfactor, a, b, t1, t2):
    t=(4/(3*lmax)) * 2**(np.arange(nScales-1,-1,-1))
    
    g = [None]*(nScales+1)
    g[0] = lambda x: kernel_meyer(t[0]*x,'sf')

    for j in range(nScales):
        g[j+1] = lambda x, t=t[j]: kernel_meyer(t*x,'wavelet')
    return g

def log_scales(lmin,lmax,nScales,t1=1,t2=2):
  smin=t1/lmax
  smax=t2/lmin
  return np.exp(np.linspace(np.log(smax),np.log(smin),nScales))
  
def kernel_abspline3(x, alpha, beta, t1, t2):
    x = np.array(x)
    r = np.zeros(x.shape)
#    M=[[1, t1, t1**2, t1**3], [1, t2, t2**2, t2**3], [0, 1, 2*t1, 3*t1**2], [0, 1, 2*t2, 3*t2**2]]
#    v=[[1] , [1] , [t1**(-alpha) * alpha * t1**(alpha-1)] , [-beta*t2**(-beta-1)*t2**beta]]
#    a=np.linalg.solve(M,v)   
    a = np.array([-5, 11, -6, 1])
    
    r1 = (x>=0) & (x<t1)
    r2 = (x>=t1) & (x<t2)
    r3 = x>=t2
    
    r[r1] = x[r1]**alpha * t1**(-alpha)
    r[r2] = a[0] + a[1]*x[r2] + a[2]*x[r2]**2 + a[3]*x[r2]**3
    r[r3] = x[r3]**(-beta) * t2**(beta)
    
    return r

def kernel_meyer(x, kerneltype):
    l1=2/3
    l2=4/3  #2*l1
    l3=8/3  #4*l1;
    
    v = lambda x: x**4 *(35 - 84*x + 70* x**2 - 20* x**3) ; 

    r1 = (x>=0) & (x<l1)
    r2 = (x>=l1) & (x<l2)
    r3 = (x>=l2) & (x<l3)
        
    # as we initialize r with zero, computed function will implicitly be zero for
    # all x not in one of the three regions defined above
    r = np.zeros(x.shape)
    if kerneltype=='sf':
        r[r1] = 1
        r[r2] = np.cos( (np.pi/2) * v(np.abs(x[r2]) /l1 - 1))
    elif  kerneltype=='wavelet':
        r[r2] = np.sin((np.pi/2) * v(np.abs(x[r2])/l1 -1))
        r[r3] = np.cos((np.pi/2) * v(np.abs(x[r3])/l2 -1))
    return r