"""
LAMP
"""

import numpy as np

tol = 1.e-6    # zero tolerance

def project_(x, xs, ys):
    """
    Projection
    """
    assert (type(x) is np.ndarray) and (type(xs) is np.ndarray) and (type(ys) is np.ndarray), \
        "*** ERROR (Force-Scheme): project input must be numpy.array type."

    ninst, dim = x.shape    # number os instances, data dimension
    k, a = xs.shape         # number os sample instances
    p = ys.shape[1]         # visual space dimension
    
    assert dim == a, "*** LAMP Error: x and xs dimensions must be egual."

    Y = np.zeros((ninst, p))
    for pt in range(ninst):
        # computes alphas
        alpha = np.zeros(k)
        for i in range(k):
            # verify if the point to be projectec is a control point
            # avoids division by zero
            if np.linalg.norm(xs[i] - x[pt]) < tol:
                alpha[i] = np.finfo(float).max
            else:
                alpha[i] = 1 / np.linalg.norm(xs[i] - x[pt])**2
        
        # computes x~ and y~ (eq 3)
        xtilde = np.zeros(dim)
        ytilde = np.zeros(p)
        for i in range(k):
            xtilde += alpha[i] * xs[i]
            ytilde += alpha[i] * ys[i]
        xtilde /= np.sum(alpha)
        ytilde /= np.sum(alpha)

        A = np.zeros((k, dim))
        B = np.zeros((k, p))
        xhat = np.zeros((k, dim))
        yhat = np.zeros((k, p))
        # computation of x^ and y^ (eq 6)
        for i in range(k):
            xhat[i] = xs[i] - xtilde
            yhat[i] = ys[i] - ytilde
            A[i] = np.sqrt(alpha[i]) * xhat[i]
            B[i] = np.sqrt(alpha[i]) * yhat[i]
    
        U, D, V = np.linalg.svd(np.dot(A.T, B)) # (eq 7)
        # VV is the matrix V filled with zeros
        VV = np.zeros((dim, p)) # size of U = dim, by SVD
        for i in range(p): # size of V = p, by SVD
             VV[i,range(p)] = V[i]
        
        M = np.dot(U, VV) # (eq 7)

        Y[pt] = np.dot(x[pt] - xtilde, M) + ytilde # (eq 8)

    return Y

def project(x, xs, ys):
    """
    Projection
    """
    assert (type(x) is np.ndarray) and (type(xs) is np.ndarray) and (type(ys) is np.ndarray), \
        "*** ERROR (Force-Scheme): project input must be numpy.array type."

    ninst, dim = x.shape    # number os instances, data dimension
    k, a = xs.shape         # number os sample instances
    p = ys.shape[1]         # visual space dimension
    
    assert dim == a, "*** LAMP Error: x and xs dimensions must be egual."

    Y = np.zeros((ninst, p))

    sqnorm = lambda x: (x * x).sum(axis=1)

    for pt in range(ninst):
        # computes alphas
        alpha = np.zeros(k)
        xsxpt = xs - x[pt]
        nxspt = sqnorm(xsxpt) #np.linalg.norm(xsxpt, axis=1)**2
        alpha = 1 / nxspt
        alpha[nxspt < tol] = np.finfo(float).max
        
        # computes x~ and y~ (eq 3)
        sumAlpha = sum(alpha)
        xtilde = np.dot(alpha,xs)
        ytilde = np.dot(alpha,ys)

        xtilde /= sumAlpha
        ytilde /= sumAlpha

        sqrtAlpha = np.sqrt(alpha)
        xhat = xs - xtilde
        yhat = ys - ytilde
        
        A = np.zeros((k, dim))
        B = np.zeros((k, p))
        for j in range(dim):
            A[:,j] = sqrtAlpha*xhat[:,j]
        for j in range(p):
            B[:,j] = sqrtAlpha*yhat[:,j]
    
        U, D, V = np.linalg.svd(np.dot(A.T, B)) # (eq 7)
        # VV is the matrix V filled with zeros
        VV = np.zeros((dim, p))
        VV[:p,:p] = V

        M = np.dot(U, VV) # (eq 7)

        Y[pt] = np.dot(x[pt] - xtilde, M) + ytilde # (eq 8)

    return Y
    

def plot(y, t):
    import matplotlib.pyplot as mpl
    mpl.scatter(y.T[0], y.T[1], c = t)
    mpl.show()


