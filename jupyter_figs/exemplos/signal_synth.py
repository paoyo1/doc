import sys
import numpy as np
import exemplos.plot_graph as pg
import matplotlib.pyplot as plt
from exemplos.GFT import GFT
from functools import reduce

##########
# How to run the examples:
# python3 signal_synth.py rgd.adj rgd.xy
# python3 signal_synth.py pert400+.adj pert400+.xy
#########

A = np.loadtxt(sys.argv[1])
p = np.loadtxt(sys.argv[2])

D = np.diag(np.sum(A,axis=0))
L = D - A


###########
# Synthetized function
###########
#gft = GFT(L)
#S = gft.get_spec()
##fs = np.exp(-(S**2)/0.1)
##mp = np.ceil(np.amax(S)/2)
##fs = np.exp(-(S**2)/0.3) + np.exp(-(S-mp)**2)
##fs = np.ones((S.shape[0],1))
#fs = np.zeros((p.shape[0],1))
#fs[50]=1
#gft.set_kernel(fs)
#gs = gft.synthetize()
#
#
#pg.plot_specf(S,fs,1)
#pg.plot_graph_s(A,p,gs,2)


###########
# Uncertainty principle
###########
#n = p.shape[0]
#fs = np.zeros((n,1))
#pc = 455
#for i in range(0,n):
#    fs[i] = np.exp(-(np.linalg.norm(p[i,:]-p[pc,:])**2)/0.5)
#
#gft = GFT(L)
#gft.set_signal(fs)
#hfs = gft.gft()
#S = gft.get_spec()
#pg.plot_specf(S,hfs,1)
#pg.plot_graph_s(A,p,fs,2)

############
## Filtering a Step Function
############
#n = p.shape[0]
#fs = np.zeros((n,1))
#fs[n//2:,0] = 1.0
#fsp = fs + np.random.uniform(low=-0.3, high=0.3, size=(n,1))
#
##### original #####
#gft = GFT(L)
#gft.set_signal(fs)
#spc_fs = gft.gft()
#
##### Perturbed #####
#gftp = GFT(L)
#gftp.set_signal(fsp)
#spc_fsp = gftp.gft()
#
#
##### Perturbed #####
#S = gft.get_spec()
#h = np.exp(np.divide(-(4*S)**2,2.0*S[-1]))
#h2 = np.exp(np.divide(-(3*(S[-1]-S))**2,2.0*S[-1]))
##he = h
#he = (h+h2)
#spc_fspf = gftp.filtering(he.ravel())
#
#gftpf = GFT(L)
#gftpf.set_gft_as_kernel(spc_fspf)
#ifspf = gftpf.igft()
#
##pg.plot_specf(S,fst,1)
#pg.plot_specf(S,spc_fsp,1)
###pg.plot_graph_s(A,p,ifs,2)
##pg.plot_graph_s(A,p,fs,4)
##pg.plot_specf(S,h,3)
##pg.plot_specf(S,he,4)
#plt.figure(3)
#plt.plot(S,h)
#plt.figure(4)
#plt.plot(S,he)
#pg.plot_specf(S,spc_fspf,5)
#
#pg.plot_graph_s(A,p,fs,7)
#pg.plot_graph_s(A,p,fsp,2)
#pg.plot_graph_s(A,p,ifspf,6)

############
## Filtering Coordinates
############
n = p.shape[0]
fsx = p[:,0]
fsy = p[:,1]

#### original #####
gftx = GFT(L)
gftx.set_signal(fsx)
spc_fsx = gftx.gft()

gfty = GFT(L)
gfty.set_signal(fsy)
spc_fsy = gfty.gft()

#### Low Pass X #####
S = gftx.get_spec()
h = np.exp(np.divide(-(2*S)**2,2.0*S[-1]))
he = h
#h2 = np.exp(np.divide(-(3*(S[-1]-S))**2,2.0*S[-1]))
#he = h+h2
spc_fsxf = gftx.filtering(he.ravel())

gftfsxf = GFT(L)
gftfsxf.set_gft_as_kernel(spc_fsxf)
ifsxf = gftfsxf.igft()

#### Low Pass y #####
spc_fsyf = gfty.filtering(he.ravel())

gftfsyf = GFT(L)
gftfsyf.set_gft_as_kernel(spc_fsyf)
ifsyf = gftfsyf.igft()


pn=np.zeros((p.shape[0],2))
pn[:,0]=ifsxf[:,0]
pn[:,1]=ifsyf[:,0]

pnx=np.zeros((p.shape[0],2))
pnx[:,0]=ifsxf[:,0]
pnx[:,1]=p[:,1]

pny=np.zeros((p.shape[0],2))
pny[:,0]=p[:,0]
pny[:,1]=ifsyf[:,0]

#------------

#pg.plot_specf(S,spc_fsx,1)
#plt.figure(2)
#plt.plot(S,he)
#pg.plot_specf(S,spc_fsxf,3)

pg.plot_graph_s(A,p,fsx,4)
#pg.plot_graph_s(A,pn,fsx,5)
pg.plot_graph_s(A,pnx,fsx,6)
pg.plot_graph_s(A,pny,fsx,7)

xmin = -1
xmax = 3
ymin = -0.5
ymax = 2

fig = plt.figure(8)
ax = fig.add_subplot(111)
ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)

plt.scatter(pn[:,0],pn[:,1])

fig = plt.figure(9)
ax = fig.add_subplot(111)
ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)

plt.scatter(p[:,0],p[:,1])

fig = plt.figure(10)
ax = fig.add_subplot(221)
plt.hist(p[:,0],6,facecolor='gray')
ax = fig.add_subplot(222)
plt.hist(p[:,1],6,facecolor='gray')
ax = fig.add_subplot(223)
plt.hist(pn[:,0],6,facecolor='gray')
ax = fig.add_subplot(224)
plt.hist(pn[:,1],6,facecolor='gray')


###########
# Graph Fourier of a Gaussian
###########
#n = p.shape[0]
#fs = np.zeros((n,1))
#U = np.array([[1,0],[0,1]])
#U = np.matrix(np.sqrt(2)*U)
#D = np.matrix(np.array([[20,0],[0,20]]))
#Di = D.I
#V = reduce(np.dot,[U,Di,U.transpose()])
#pc = 208
#for i in range(0,n):
#    x = p[i,:]-p[pc,:]
#    e = reduce(np.dot,[x.transpose(),V,x])
#    fs[i] = np.exp(-e) #+ np.random.normal(0, 0.1, 1)
#
#gft = GFT(L)
#gft.set_signal(fs)
#fst = gft.gft()
#ifs = gft.igft()
#S = gft.get_spec()
#pg.plot_specf(S,fst,1)
##pg.plot_graph_s(A,p,ifs,2)
#pg.plot_graph_s(A,p,fs,3)


###########
# Translation
###########
#gft = GFT(L)
#S = gft.get_spec()
#fs = np.exp(-(S**2)/0.1)
#gft.set_gft_as_kernel(fs)
#fs_trans = gft.translation(155)
#fs_trans2 = gft.translation(555)
#pg.plot_specf(S,fs,1)
#pg.plot_graph_s(A,p,fs_trans,2)
#pg.plot_graph_s(A,p,fs_trans2,4)
#
#ifs = gft.igft()
#pg.plot_graph_s(A,p,ifs,3)

#gft2 = GFT(L)
#S2 = gft2.get_spec()
#gft2.set_signal(ifs)
#gfifs = gft2.gft()
#pg.plot_specf(S,gfifs,4)

plt.show()
